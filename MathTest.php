<?php

require_once 'Math.php';

/**
 * Created by PhpStorm.
 * User: adria
 * Date: 24.11.2015
 * Time: 19:02
 */
class MathTest extends PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider integerDataProvider
     */
    public function testSumIntegersForNumbers($a, $b, $expected)
    {
        $math = new Math();
        $result = $math->sum($a, $b);

        $this->assertEquals($expected, $result);
    }

    /**
     * @dataProvider invalidDataProvider
     *
     * @expectedException InvalidArgumentException
     */
    public function testSumThrowsExceptionForInvalidData($a, $b)
    {
        $math = new Math();
        $result = $math->sum($a, $b);
    }

    public function integerDataProvider() {
        return array(
            array(1,2,3),
            array(0,0,0),
            array(-1,-3,-4),
        );
    }

    public function invalidDataProvider() {
        return array(
            array(1,'abc'),
            array('a',0),
            array('as','df'),
            array(array(),1),
        );
    }
}
